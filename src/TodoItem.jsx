import React, { useState } from 'react';
import { Button, TextInput, Modal } from 'flowbite-react';
import { HiPlusCircle } from 'react-icons/hi';

export const TodoItemAdd = ({ addTask }) => {
  const [value, setValue] = useState("");

  const handleSubmit = e => {
    e.preventDefault();
    value && addTask(value)

    setValue("");
  };

  return (
    <form onSubmit={handleSubmit} className='flex max-w-md flex-col gap-4'>
      <div>
        <TextInput
          value={value}
          id="todo-item"
          type="text"
          placeholder="Enter task"
          onChange={e => setValue(e.target.value)}
        />
      </div>
      <Button type="submit">
        <HiPlusCircle />
      </Button>
    </form>
  );
}

export const TodoItemEdit = ({
  openModal,
  setOpenModal,
  editTask,
  editItem,
  editedData,
  setEditedData,
  value
}) => {

  return (
    <Modal
      dismissible 
      show={openModal} 
      onClose={() => setOpenModal(false)}
    >
      <Modal.Header>Edit Task: <strong>{editItem.value}</strong></Modal.Header>
      <Modal.Body>
        <div>
          <TextInput
            id="todo-item"
            type="text"
            placeholder="Enter task"
            onChange={e => setEditedData(e.target.value)}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => {
          editTask(editItem.index, editedData)
          setOpenModal(false)
        }}>
          Edit
        </Button>
        <Button
          color="gray"
          onClick={() => setOpenModal(false)}
        >
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  )
}