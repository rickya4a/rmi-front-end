import React, { useState } from "react";
import { TodoItemAdd, TodoItemEdit } from './TodoItem'
import { Checkbox } from 'flowbite-react';
import { HiOutlineTrash, HiOutlinePencil } from 'react-icons/hi';

export const TodoList = () => {
  const [tasks, setTasks] = useState([
    {
      text: "Learn ReactJs",
      isCompleted: true
    }
  ]);

  const [openModal, setOpenModal] = useState(false);
  const [editItem, setEditItem] = useState({})
  const [editedData, setEditedData] = useState('')

  const editTask = (index, newName) => {
    tasks[index] = {
      ...tasks[index],
      text: newName
    }

    return true
  }

  const addTask = text => setTasks([...tasks, { text, isCompleted: false }]);

  const toggleTask = index => {
    const newTasks = [...tasks];
    newTasks[index].isCompleted = !newTasks[index].isCompleted;

    setTasks(newTasks);
  };

  const removeTask = index => {
    const newTasks = [...tasks];
    newTasks.splice(index, 1);

    setTasks(newTasks);
  };

  return (
    <>
      <h1>React To-do List</h1>
      <div className="mx-auto w-60 md:w-72 lg:w-96">
        {tasks.map((task, index) => (
          <div
            className="flex px-4 py-3 items-center rounded mb-2 justify-between bg-white text-black text-sm"
            key={index}
          >
            <Checkbox 
              className="bg-slate-400 checked:bg-blue-500" 
              checked={task.isCompleted}
              onChange={() => toggleTask(index)} />
            <span
              onClick={() => toggleTask(index)}
              className={`
                cursor-pointer
                ${task.isCompleted ? 'todo-completed' : ''} 
            `}>
              {task.text}
            </span>

            <div className="flex justify-between space-x-2 text-lg">
              <HiOutlinePencil
                className="cursor-pointer text-blue-500 hover:text-blue-300"
                onClick={() => {
                  setEditItem({ index, value: task.text })
                  setOpenModal(!openModal)
                }}
              />
              <HiOutlineTrash
                className="cursor-pointer text-red-500 hover:text-red-300"
                onClick={() => removeTask(index)} />
            </div>
          </div>
        ))}

        <TodoItemAdd addTask={addTask} />

        <TodoItemEdit 
          openModal={openModal}
          setOpenModal={setOpenModal}
          editTask={editTask}
          editItem={editItem}
          editedData={editedData}
          setEditedData={setEditedData}
        />
      </div>
    </>
  );
}